import { Injectable } from '@angular/core';

//import models
import { Student } from "../../Models/student";

//importando localstorage
import { Storage } from '@ionic/storage';

/*
  Generated class for the StudentProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class StudentProvider {

  private KEY = "student";

  students:Student[];

  constructor(public storage:Storage) {
  }

  //Guardo en el archivo el profesor
  saveStorage(students:Student[]){
    this.storage.remove(this.KEY);
    this.storage.set(this.KEY, students);
  }

  loadStorage(){
    this.students = [];

    this.storage.get(this.KEY).then((value) => {

      value.forEach(element => {
        this.students.push(element);
      });
    });
  }

  getStundets(){
    this.loadStorage();

    return this.students;
  }

  setStudents(student:Student){
    this.students.push(student);

    this.saveStorage(this.students);
  }

  deleteStudent(index:number){
    let students:Student[] = [];

    this.students.forEach((element, i) => {
      if(i != index){
        students.push(element);
      }
    });

    this.saveStorage(students);
  }

  findStudent(index:number){
    return this.students[index];
  }

  editStudent(index:number, student:Student){
    this.students[index] = student;

    this.saveStorage(this.students);
  }

}
