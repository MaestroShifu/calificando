import { Injectable } from '@angular/core';

//import los modelos
import { Note } from "../../Models/note";

//importando localstorage
import { Storage } from '@ionic/storage';

/*
  Generated class for the NoteProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class NoteProvider {

  private KEY = "note";

  notes:Note[];

  constructor(public storage:Storage) {
  }

  //Guardo en el archivo el profesor
  saveStorage(notes:Note[]){
    this.storage.remove(this.KEY);
    this.storage.set(this.KEY, notes);
  }

  loadStorage(){
    this.notes = [];

    this.storage.get(this.KEY).then((value) => {

      value.forEach(element => {
        this.notes.push(element);
      });
    });
  }

  getNotes(){
    this.loadStorage();

    return this.notes;
  }

  setNote(note:Note){
    this.notes.push(note);

    this.saveStorage(this.notes);
  }

  deleteNote(index:number){
    let notes:Note[] = [];

    this.notes.forEach((element, i) => {
      if(i != index){
        notes.push(element);
      }
    });

    this.saveStorage(notes);
  }

  findNote(index:number){
    return this.notes[index];
  }

  editNote(index:number, note:Note){
    this.notes[index] = note;

    this.saveStorage(this.notes);
  }

}
