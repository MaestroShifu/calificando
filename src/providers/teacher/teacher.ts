import { Injectable } from '@angular/core';

//importando localstorage
import { Storage } from '@ionic/storage';

//importando el modelo
import { Teacher } from '../../Models/teacher';

/*
  Generated class for the TeacherProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class TeacherProvider {

  private KEY = "teacher";

  teacher:Teacher = {
    name: "",
    curse: "",
    bio: ""
  };

  constructor(public storage:Storage) {

  }

  //Guardo en el archivo el profesor
  public saveStorage(teacher:Teacher){
    this.storage.set(this.KEY, teacher);
  }

  public loadStorage(){
    this.storage.get(this.KEY).then((value) => {
      this.teacher = value;
    });
  }

  public getTeacher(){
    this.loadStorage();

    return this.teacher;
  }

  setTeacher(teacher:Teacher){
    this.teacher = teacher;

    this.saveStorage(this.teacher);
  }

}
