import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

//paginas
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';
import { TeacherPage } from '../pages/teacher/teacher';
import { TeacherEditPage } from "../pages/teacher-edit/teacher-edit";
import { NotePage } from "../pages/note/note";
import { NoteCreatePage } from "../pages/note-create/note-create";
import { NoteEditPage } from "../pages/note-edit/note-edit";
import { StudentPage } from "../pages/student/student";
import { StudentCreatePage } from "../pages/student-create/student-create";
import { StudentEditPage } from "../pages/student-edit/student-edit";
import { StudentShowPage } from "../pages/student-show/student-show";
import { StudentNotePage } from "../pages/student-note/student-note";


import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

//LocalStorage
import { IonicStorageModule } from '@ionic/storage';

//import providers
import { TeacherProvider } from '../providers/teacher/teacher';
import { NoteProvider } from '../providers/note/note';
import { StudentProvider } from '../providers/student/student';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    TabsPage,
    TeacherPage,
    TeacherEditPage,
    NotePage,
    NoteCreatePage,
    NoteEditPage,
    StudentPage,
    StudentCreatePage,
    StudentEditPage,
    StudentShowPage,
    StudentNotePage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    TabsPage,
    TeacherPage,
    TeacherEditPage,
    NotePage,
    NoteCreatePage,
    NoteEditPage,
    StudentPage,
    StudentCreatePage,
    StudentEditPage,
    StudentShowPage,
    StudentNotePage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    TeacherProvider,
    NoteProvider,
    StudentProvider
  ]
})
export class AppModule {}
