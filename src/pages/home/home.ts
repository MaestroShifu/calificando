import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

//import model
import { Student } from '../../Models/student';

//import provider
import { StudentProvider } from "../../providers/student/student";

//import page
import { StudentShowPage } from "../student-show/student-show";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  students:Student[] = [];

  constructor(public navCtrl: NavController, public studentProvider:StudentProvider) {
    // this.start();
  }

  start(){
    this.students = this.studentProvider.getStundets();
  }

  ionViewWillEnter(){
    this.start();
  }

  showStudent(index:number){
    this.navCtrl.push(StudentShowPage, {index: index});
  }

}
