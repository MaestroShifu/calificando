import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TeacherEditPage } from './teacher-edit';

@NgModule({
  declarations: [
    TeacherEditPage,
  ],
  imports: [
    IonicPageModule.forChild(TeacherEditPage),
  ],
})
export class TeacherEditPageModule {}
