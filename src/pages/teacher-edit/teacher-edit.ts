import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

//import formularios
import { Validators, FormBuilder, FormGroup } from '@angular/forms';

//Import providers
import { TeacherProvider } from '../../providers/teacher/teacher';

//import models
import { Teacher } from "../../Models/teacher";

/**
 * Generated class for the TeacherEditPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-teacher-edit',
  templateUrl: 'teacher-edit.html',
})
export class TeacherEditPage {
  public formGroup: FormGroup;

  teacher:Teacher = {
    name: "",
    curse: "",
    bio: ""
  };

  constructor(public navCtrl: NavController, public navParams: NavParams, public formBuilder: FormBuilder, public teacherProvider:TeacherProvider) {
    this.start();
  }

  start(){
    this.teacher = this.teacherProvider.getTeacher();

    this.formGroup = this.formBuilder.group({
      //['nombre por defecto', [array de validaciones]]
      name: [this.teacher.name, [Validators.required, Validators.minLength(3), Validators.maxLength(20)]],
      curse: [this.teacher.curse, [Validators.required, Validators.minLength(3), Validators.maxLength(16)]],
      bio: [this.teacher.bio, [ Validators.minLength(3), Validators.maxLength(200)]],
    });
  }

  ionViewDidLoad() {

  }

  saveTeacher(value){

    this.teacher.name = value.name;
    this.teacher.curse = value.curse;
    this.teacher.bio = value.bio;

    this.teacherProvider.setTeacher(this.teacher);

    this.navCtrl.pop();
  }

}
