import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { Validators, FormBuilder, FormGroup } from '@angular/forms';

//import provider
import { NoteProvider } from "../../providers/note/note";

import { Note } from "../../Models/note";
/**
 * Generated class for the NoteEditPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-note-edit',
  templateUrl: 'note-edit.html',
})
export class NoteEditPage {

  formGroup:FormGroup;
  note:Note;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public formBuilder: FormBuilder, public noteProvider:NoteProvider) {

    this.note = noteProvider.findNote(navParams.get('index'));

    this.start();
  }

  ionViewDidLoad() {
  }

  start(){
    this.formGroup = this.formBuilder.group({
      //['nombre por defecto', [array de validaciones]]
      name: [this.note.name, [Validators.required, Validators.minLength(4), Validators.maxLength(16)]],
      value: [this.note.value, [Validators.required]],
    });
  }

  editNote(value){
    let note:Note = {
      name: value.name,
      value: value.value
    };

    this.noteProvider.editNote(this.navParams.get('index'), note);

    this.navCtrl.pop();
  }

}
