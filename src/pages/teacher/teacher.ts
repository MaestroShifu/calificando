import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

//import modelo profesor
import { Teacher } from "../../Models/teacher";

//import provider
import { TeacherProvider } from "../../providers/teacher/teacher";

//import pages
import { TeacherEditPage } from "../teacher-edit/teacher-edit";

/**
 * Generated class for the TeacherPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-teacher',
  templateUrl: 'teacher.html',
})
export class TeacherPage {

  teacher:Teacher = {
    name: "",
    curse: "",
    bio: ""
  };

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public teacherProvider:TeacherProvider) {
      this.start();
  }

  start(){
    this.teacher = this.teacherProvider.getTeacher();
  }

  ionViewDidLoad() {
  }

  ionViewWillEnter(){
    // this.start();
  }

  teacherEdit(){
    this.navCtrl.push(TeacherEditPage);
  }

}
