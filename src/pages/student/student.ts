import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

//import provider
import { StudentProvider } from "../../providers/student/student";

//import model
import { Student } from "../../Models/student";

//mport page
import { StudentCreatePage } from "../student-create/student-create";
import { StudentEditPage } from "../student-edit/student-edit";
import { StudentShowPage } from "../student-show/student-show";

/**
 * Generated class for the StudentPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-student',
  templateUrl: 'student.html',
})
export class StudentPage {

  students:Student[] = [];

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public studentProvider:StudentProvider) {

  }

  //inicio de la carga de la pantalla
  start(){
    this.students = [];
    this.students = this.studentProvider.getStundets();
  }

  ionViewDidLoad() {

  }

  ionViewWillEnter(){
    this.start();
  }

  createStundet(){
    this.navCtrl.push(StudentCreatePage);
  }

  deleteStundet(index:number){
    this.studentProvider.deleteStudent(index);

    this.start();
  }

  editStudent(index:number){
    this.navCtrl.push(StudentEditPage, {index: index})
  }

  showStudent(index:number){
    this.navCtrl.push(StudentShowPage, {index: index});
  }

}
