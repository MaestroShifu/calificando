import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

//import form
import { Validators, FormBuilder, FormGroup } from '@angular/forms';

//import provider
import { NoteProvider } from "../../providers/note/note";

//import model
import { Note } from "../../Models/note";

/**
 * Generated class for the NoteCreatePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-note-create',
  templateUrl: 'note-create.html',
})
export class NoteCreatePage {

  formGroup:FormGroup;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public formBuilder: FormBuilder, public noteProvider:NoteProvider) {

    this.start();
  }

  ionViewDidLoad() {
  }

  start(){
    this.formGroup = this.formBuilder.group({
      //['nombre por defecto', [array de validaciones]]
      name: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(16)]],
      value: ['', [Validators.required]],
    });
  }

  createNote(value){
    let note:Note = {
      name: value.name,
      value: value.value
    };

    this.noteProvider.setNote(note);

    this.navCtrl.pop();
  }

}
