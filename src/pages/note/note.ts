import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

//import model
import { Note } from "../../Models/note";

//Import provider
import { NoteProvider } from "../../providers/note/note";

//import pages
import { NoteCreatePage } from "../note-create/note-create";
import { NoteEditPage } from "../note-edit/note-edit";

/**
 * Generated class for the NotePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-note',
  templateUrl: 'note.html',
})
export class NotePage {

  notes:Note[] = [];

  constructor(public navCtrl: NavController, public navParams: NavParams,
     public noteProvider:NoteProvider) {

  }

  start(){
    this.notes = this.noteProvider.getNotes();
  }

  ionViewDidLoad() {
  }

  ionViewWillEnter(){
    this.start();
  }

  createNote(){
    this.navCtrl.push(NoteCreatePage);
  }

  deleteNote(index:number){
    this.noteProvider.deleteNote(index);

    this.start();
  }

  editNote(index:number){
    this.navCtrl.push(NoteEditPage, {index: index});
  }

}
