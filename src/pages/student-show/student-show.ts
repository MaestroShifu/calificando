import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

//import models
import { Student } from "../../Models/student";

//import providers
import { StudentProvider } from "../../providers/student/student";

//import page
import { StudentNotePage } from "../student-note/student-note";

/**
 * Generated class for the StudentShowPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-student-show',
  templateUrl: 'student-show.html',
})
export class StudentShowPage {

  student:Student;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public studentProvider:StudentProvider) {
      this.start();
  }

  start(){
    this.student = this.studentProvider.findStudent(this.navParams.get('index'));
  }

  ionViewDidLoad() {
  }

  ionViewWillEnter(){
    this.start();
  }

  noteEdit(idNote:number){

    this.navCtrl.push(StudentNotePage, {idStudent: this.navParams.get('index'), idNote: idNote});
  }

}
