import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { StudentShowPage } from './student-show';

@NgModule({
  declarations: [
    StudentShowPage,
  ],
  imports: [
    IonicPageModule.forChild(StudentShowPage),
  ],
})
export class StudentShowPageModule {}
