import { Component } from '@angular/core';

import { HomePage } from '../home/home';

//import paginas
// import { TeacherPage } from "../teacher/teacher";
import { NotePage } from "../note/note";
import { StudentPage } from "../student/student";

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = HomePage;
  // tab2Root = TeacherPage;
  tab3Root = NotePage;
  tab4Root = StudentPage;

  constructor() {

  }
}
