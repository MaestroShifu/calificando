import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

//import forms validator
import { Validators, FormBuilder, FormGroup } from '@angular/forms';

//import models
import { Student } from "../../Models/student";

//import provider
import { StudentProvider } from "../../providers/student/student";

/**
 * Generated class for the StudentEditPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-student-edit',
  templateUrl: 'student-edit.html',
})
export class StudentEditPage {

  formGroup: FormGroup;
  student:Student;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public formBuilder: FormBuilder, public stundetProvider:StudentProvider) {

    this.student = stundetProvider.findStudent(this.navParams.get('index'));

    this.start();
  }

  start(){
    this.formGroup = this.formBuilder.group({
      //['nombre por defecto', [array de validaciones]]
      name: [this.student.name , [Validators.required, Validators.minLength(4), Validators.maxLength(16)]],
      carrer: [this.student.carrer, [Validators.required, Validators.minLength(4), Validators.maxLength(16)]],
    });
  }

  ionViewDidLoad() {

  }

  saveStudent(value){
    this.student.name = value.name;
    this.student.carrer = value.carrer;

    this.stundetProvider.editStudent(this.navParams.get('index'), this.student);

    this.navCtrl.pop();
  }

}
