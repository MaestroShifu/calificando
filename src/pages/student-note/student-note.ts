import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

//import forms validator
import { Validators, FormBuilder, FormGroup } from '@angular/forms';

//import provider
import { StudentProvider } from '../../providers/student/student';

//import models
import { Student } from "../../Models/student";
import { NoteStundet } from "../../Models/note";

/**
 * Generated class for the StudentNotePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-student-note',
  templateUrl: 'student-note.html',
})
export class StudentNotePage {

  formGroup: FormGroup;

  student:Student;
  note:NoteStundet;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public formBuilder: FormBuilder, public stundetProvider:StudentProvider) {
      this.start();
  }

  start(){
    this.student = this.stundetProvider.findStudent(this.navParams.get('idStudent'));

    this.note = this.student.notes[this.navParams.get('idNote')];

    this.formGroup = this.formBuilder.group({
      //['nombre por defecto', [array de validaciones]]
      valueStudent: [this.note.valueStudent, [Validators.required]],
    });
  }

  ionViewDidLoad() {

  }

  saveNote(value){
    this.note.valueStudent = value.valueStudent;

    //sumatoria de notas
    let sum:number = 0;
    this.student.notes.forEach(element => {
      sum = (+sum) + (+element.valueNote);
    });

    this.note.value = (+this.note.valueStudent) * (+this.note.valueNote) / (+sum);

    let sumValue:number = 0;
    this.student.notes.forEach(element => {
      sumValue = (+sumValue) + (+element.value);
    });

    this.student.total = sumValue;

    this.student.notes[this.navParams.get('idNote')] = this.note;
    this.stundetProvider.editStudent(this.navParams.get('idStudent'), this.student);

    this.navCtrl.pop();
  }

}
