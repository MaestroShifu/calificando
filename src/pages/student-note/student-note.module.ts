import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { StudentNotePage } from './student-note';

@NgModule({
  declarations: [
    StudentNotePage,
  ],
  imports: [
    IonicPageModule.forChild(StudentNotePage),
  ],
})
export class StudentNotePageModule {}
