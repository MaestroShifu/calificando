import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

//import forms validator
import { Validators, FormBuilder, FormGroup } from '@angular/forms';

//import models
import { Student } from "../../Models/student";
import { Note, NoteStundet } from "../../Models/note";

//import provider
import { StudentProvider } from "../../providers/student/student";
import { NoteProvider } from "../../providers/note/note";

/**
 * Generated class for the StudentCreatePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-student-create',
  templateUrl: 'student-create.html',
})
export class StudentCreatePage {
  formGroup: FormGroup;
  note:Note[] = [];

  constructor(public navCtrl: NavController, public navParams: NavParams,
              public formBuilder: FormBuilder, public stundetProvider:StudentProvider, public noteProvider:NoteProvider) {

    this.start();
  }

  start(){
    this.formGroup = this.formBuilder.group({
      //['nombre por defecto', [array de validaciones]]
      name: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(16)]],
      carrer: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(16)]],
    });

    this.note = this.noteProvider.getNotes();
  }

  ionViewDidLoad() {

  }

  saveStudent(value){
    let noteStundet:NoteStundet[] = [];

    let noteStu:NoteStundet;

    this.note.forEach(element => {
      noteStu = {
        name: element.name,
        valueNote: element.value,
        valueStudent: 0,
        value: 0
      }

      noteStundet.push(noteStu);
    });

    let student:Student = {
      name: value.name,
      carrer: value.carrer,
      notes: noteStundet,
      total: 0
    }

    this.stundetProvider.setStudents(student);

    this.navCtrl.pop();
  }

}
