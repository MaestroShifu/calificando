import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { StudentCreatePage } from './student-create';

@NgModule({
  declarations: [
    StudentCreatePage,
  ],
  imports: [
    IonicPageModule.forChild(StudentCreatePage),
  ],
})
export class StudentCreatePageModule {}
