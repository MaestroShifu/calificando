export interface Teacher{
  name: string,
  bio?: string,
  curse: string
}
