import { NoteStundet } from "../Models/note";

export interface Student{
  name: string,
  carrer: string,
  notes?: NoteStundet[],
  total?: number
}
